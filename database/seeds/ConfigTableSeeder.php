<?php

use Illuminate\Database\Seeder;

class ConfigTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('config')->insert([
            'clave_clientes' => 4,
            'clave_articulos' => 4,
            'folio_ventas' => 1,
            'tasa_financiamiento' => 2.8,
            'enganche' => 20,
            'plazo_maximo' => 12,
        ]);
    }
}
