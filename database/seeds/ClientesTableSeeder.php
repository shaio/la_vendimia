<?php

use Illuminate\Database\Seeder;

class ClientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clientes')->insert([
            'clave' => 1,
            'nombre' => 'Iván',
            'apellido_paterno' => 'Román',
            'apellido_materno' => 'Salazar',
            'rfc' => 'ROSR12312412'
        ]);

        DB::table('clientes')->insert([
            'clave' => 2,
            'nombre' => 'Anselmo',
            'apellido_paterno' => 'Marquez',
            'apellido_materno' => 'Carrilllo',
            'rfc' => 'ANMC12312412'
        ]);

        DB::table('clientes')->insert([
            'clave' => 3,
            'nombre' => 'Juan',
            'apellido_paterno' => 'Perez',
            'apellido_materno' => 'Mercado',
            'rfc' => 'MEPEJ931209-RL1'
        ]);
    }
}
