<?php

use Illuminate\Database\Seeder;

class ArticulosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('articulos')->insert([
            'clave' => 1,
            'descripcion' => 'Comedor 4 Sillas',
            'modelo' => 'Carlos V',
            'precio_articulo' => 4250.00,
            'existencia' => 3
        ]);

        DB::table('articulos')->insert([
            'clave' => 2,
            'descripcion' => 'Perchero 5',
            'modelo' => 'Victoria',
            'precio_articulo' => 1199.90,
            'existencia' => 2
        ]);

        DB::table('articulos')->insert([
            'clave' => 3,
            'descripcion' => 'Licuadora',
            'modelo' => 'Samsung',
            'precio_articulo' => 799.99,
            'existencia' => 0
        ]);
    }
}
