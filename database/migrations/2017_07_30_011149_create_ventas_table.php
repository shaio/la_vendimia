<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ventas', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('folio')->unsigned()->unique();
            $table->integer('id_cliente')->unsigned();
            $table->float('enganche', 10, 3);
            $table->float('bonificacion_enganche', 10, 3);
            $table->float('total', 10, 3);
            $table->integer('abonos');
            $table->float('total_a_pagar', 10, 3);
            $table->float('importe_abono', 10, 3);
            $table->float('importe_ahorra', 10, 3);
            
            $table->timestamps(); 

            $table->index('folio');
            $table->foreign('id_cliente')->references('id')->on('clientes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ventas');
    }
}
