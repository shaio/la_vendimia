<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticulosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articulos', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('clave')->unsigned()->unique();
            $table->string('descripcion', 200);
            $table->string('modelo', 50)->nullable();
            $table->float('precio_articulo', 10, 3);
            $table->integer('existencia')->unsigned();
            
            $table->timestamps(); 

            $table->index('clave');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('articulos');
    }
}
