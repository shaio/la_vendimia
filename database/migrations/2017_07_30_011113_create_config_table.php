<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('config', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('clave_clientes')->unsigned();
            $table->integer('clave_articulos')->unsigned();
            $table->integer('folio_ventas')->unsigned();
            $table->float('tasa_financiamiento', 10, 3);
            $table->float('enganche', 10, 3);
            $table->integer('plazo_maximo')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('config');
    }
}