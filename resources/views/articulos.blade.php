@extends('index') @section('contenido')
<div class="row">
    <div class="col-md-10">
        <h2 class="text-info">Artículos Registrados</h2>
    </div>
    <div class="col-md-2">
        <a href="{{url('/articulos/nuevo')}}" class="btn btn-info">
            <i class="fa fa-plus-circle fa-fw fa-lg text-success" aria-hidden="true"></i>Nuevo Artículo
        </a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
                <tr class="info">
                    <th>Clave Artículo</th>
                    <th colspan="2">Descripción</th>
                </tr>
            </thead>
            <tbody>
            	@foreach($articulos as $articulo)
                <tr>
					<td>{{$articulo->clave}}</td>  
					<td>{{$articulo->descripcion}}</td>
					<td>
						<a class="btn btn-sm btn-default" href="{{url('/articulos/consultar-editar')}}/{{$articulo->clave}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
					</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="text-center">
        {{$articulos->links()}}
    </div>
</div>
@stop