@extends('index') @section('contenido')
<div class="row" ng-init="main.ventas = {{$ventasArray}}">
    <div class="col-md-10">
        <h2 class="text-info">Ventas Activas</h2>
    </div>
    <div class="col-md-2">
        <a href="{{url('/ventas/nueva')}}" class="btn btn-info">
            <i class="fa fa-plus-circle fa-fw fa-lg text-success" aria-hidden="true"></i>Nueva Venta
        </a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
                <tr class="info">
                    <th>Folio Venta</th>
                    <th>Clave Cliente</th>
                    <th>Nombre</th>
                    <th>Total</th>
                    <th colspan="2">Fecha</th>
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="venta in main.ventas">
                    <td>@{{venta.folio}}</td>
                    <td>@{{venta.cliente.clave}}</td>
                    <td>@{{venta.cliente.nombre}} @{{venta.cliente.apellido_paterno}} @{{venta.cliente.apellido_materno}}</td>
                    <td>@{{venta.total | currency : ""}}</td>
                    <td ng-init="main['venta_fecha' + $index]=venta.created_at.substring(0, 10).split('-')">@{{main['venta_fecha' + $index][2]}}/@{{main['venta_fecha' + $index][1]}}/@{{main['venta_fecha' + $index][0]}}</td>
                    <td><a class="btn btn-link" href="{{url('/ventas')}}/@{{venta.folio}}"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="text-center">
        {{$ventas->links()}}
    </div>
</div>
@stop