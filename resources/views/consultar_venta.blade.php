@extends('index') @section('contenido')
<div class="row">
    {{-- Se almacena en angular la configuración actual del server --}}
    <div class="col-md-12" ng-init="main.venta={{$venta}}">
            <div class="panel panel-primary" ng-init='nuve.setInfo({{$venta}})'>
                <div class="panel-heading">
                    <h3 class="panel-title">Registro de Venta</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <span class=" text-success pull-right">
                            <strong>Folio Venta: @{{main.venta.folio}}</strong>
                        </span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="col-md-2">
                                <a href="#" class="btn btn-primary">Cliente</a>
                            </div>
                            <div class="col-md-5">
                                <input class="form-control" type="text" value="@{{main.venta.cliente.clave}} - @{{main.venta.cliente.nombre}} @{{main.venta.cliente.apellido_paterno}} @{{main.venta.cliente.apellido_materno}}" disabled>
                            </div>
                            <div class="col-md-5">
                                <strong>RFC: @{{main.venta.cliente.rfc}}</strong>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table">
                                <thead>
                                    <tr class="active">
                                        <th>Descripçión Artículo</th>
                                        <th>Modelo</th>
                                        <th>Cantidad</th>
                                        <th>Precio</th>
                                        <th>Importe</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="articulo in main.venta.articulo">
                                        <td>@{{articulo.descripcion}}</td>
                                        <td>@{{articulo.modelo}}</td>
                                        <td>
                                            <input class="form-control" type="number" value="@{{articulo.pivot.cantidad}}" disabled>
                                        </td>
                                        <td>@{{articulo.pivot.precio | currency : ""}}</td>
                                        <td>@{{articulo.pivot.importe | currency : ""}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-4 col-md-offset-8">
                            <div class="col-md-6">
                                <input class="form-control input-sm text-right" type="text" disabled value="Enganche:">
                            </div>
                            <div class="col-md-6">
                                <h5 class="text-right text-primary">@{{main.venta.enganche | currency : ""}}</h5>
                            </div>
                        </div>
                        <div class="col-md-4 col-md-offset-8">
                            <div class="col-md-6">
                                <input class="form-control input-sm text-right" type="text" disabled value="Bonificación Enganche:">
                            </div>
                            <div class="col-md-6">
                                <h5 class="text-right text-primary">@{{main.venta.bonificacion_enganche | currency : ""}}</h5>
                            </div>
                        </div>
                        <div class="col-md-4 col-md-offset-8">
                            <div class="col-md-6">
                                <input class="form-control input-sm text-right" type="text" disabled value="Total:">
                            </div>
                            <div class="col-md-6">
                                <h5 class="text-right text-primary">@{{main.venta.total | currency : ""}}</h5>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped">
                                <thead>
                                    <tr class="info">
                                        <th colspan="4" class="text-center text-info">ABONOS MENSUALES</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>@{{main.venta.abonos}} ABONOS DE</td>
                                        <td>@{{main.venta.importe_abono | currency}}</td>
                                        <td>TOTAL A PAGAR @{{main.venta.total_a_pagar | currency}}</td>
                                        <td>SE AHORRA @{{main.venta.importe_ahorra | currency}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
@stop @section('scripts')
<script src="{{asset('node_modules/bignumber.js/bignumber.min.js')}}"></script>
@stop