@extends('index') @section('contenido')
<div class="row">
    <div class="col-md-12">
        <form id="formulario-configuracion" action="{{url('/configuracion')}}" method="POST">
            {{ csrf_field() }}
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Configuración General</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6 text-right">
                                        <label class="text-success" for="tasa_financiamiento">Tasa Financiamiento:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control" type="number" min="0" step="0.01" name="tasa_financiamiento" value="{{$config->tasa_financiamiento}}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6 text-right">
                                        <label class="text-success" for="enganche">% Enganche:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control" type="number" min="0" step="0.01" name="enganche" value="{{$config->enganche}}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6 text-right">
                                        <label class="text-success" for="plazo_maximo">Plazo Máximo:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control" type="number" min="1" step="1" name="plazo_maximo" value="{{$config->plazo_maximo}}" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pull-right ">
                <a href="#" class="btn btn-success" data-toggle="modal" data-target="#modalCancelar">Cancelar</a>
                <button type="submit " class="btn btn-success ">Guardar</button>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="modalCancelar" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Salir de la Configuración</h4>
            </div>
            <div class="modal-body">
                ¿Seguro desea salir de la configuración?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">NO</button>
                <a href="{{url('/configuracion')}}" class="btn btn-danger">SI</a>
            </div>
        </div>
    </div>
</div>
@stop