@extends('index') @section('contenido')
<div class="row" ng-controller="nuevaVentaController as nuve">
    {{-- Se almacena en angular la configuración actual del server --}}
    <div class="col-md-12" ng-init="nuve.setConfig({{$config}})">
        <form action="{{url('/ventas/nueva/siguiente')}}" method="POST">
            {{ csrf_field() }}
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Registro de Ventas</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <span class=" text-success pull-right">
                            <strong>Folio Venta: {{$folio}}</strong>
                            <input type="hidden" name="folio" value="{{$folio}}">
                        </span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <a href="#" class="btn btn-primary">Cliente</a>
                            <select name="cliente" id="cliente" ng-model="nuve.cliente">
                                @foreach($clientes as $cliente)
                                <option value="{{$cliente}}">{{$cliente->clave}} - {{$cliente->nombre}} {{$cliente->apellido_paterno}} {{$cliente->apellido_materno}}</option>
                                @endforeach
                            </select>
                            <strong>@{{nuve.cliente_rfc}}</strong>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <a href="#" class="btn btn-primary">Artículo</a>
                            <select id="articulo" required ng-model="nuve.nuevo_articulo">
                                @foreach($articulos as $articulo)
                                <option value="{{$articulo}}">{{$articulo->descripcion}}</option>
                                @endforeach
                            </select>
                            <a href="#" class="btn btn-default" ng-click="nuve.agregarArticulo()"><i class="fa fa-plus fa-lg" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table">
                                <thead>
                                    <tr class="active">
                                        <th>Descripçión Artículo</th>
                                        <th>Modelo</th>
                                        <th>Cantidad</th>
                                        <th>Precio</th>
                                        <th>Importe</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="articulo in nuve.articulos">
                                        <td>@{{articulo.descripcion}}</td>
                                        <td>@{{articulo.modelo}}</td>
                                        <td>
                                            <input type="number" ng-model="articulo.cantidad" ng-change="nuve.calcularTotalesEImportes()" min="1" max="@{{articulo.existencia}}">
                                        </td>
                                        <td>@{{articulo.precioStr | currency : ""}}</td>
                                        <td>@{{articulo.importeStr | currency : ""}}</td>
                                        <td><a href="#" class="btn btn-link" ng-click="nuve.eliminarArticulo($index)"><i class="fa fa-times fa-2x text-danger" aria-hidden="true"></i></a></td>
                                        <input type="hidden" name="articulos[@{{$index}}]" value="@{{articulo}}" />
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <hr>
                    <div class="row" ng-if="nuve.articulos.length > 0">
                        <div class="col-md-4 col-md-offset-8">
                            <div class="col-md-6">
                                <input class="form-control input-sm text-right" type="text" disabled value="Enganche:">
                            </div>
                            <div class="col-md-6">
                                <h5 class="text-right text-primary">@{{nuve.engancheStr | currency : ""}}</h5>
                                <input type="hidden" name="enganche" value="@{{nuve.engancheStr}}">
                            </div>
                        </div>
                        <div class="col-md-4 col-md-offset-8">
                            <div class="col-md-6">
                                <input class="form-control input-sm text-right" type="text" disabled value="Bonificación Enganche:">
                            </div>
                            <div class="col-md-6">
                                <h5 class="text-right text-primary">@{{nuve.bonificacionEngancheStr | currency : ""}}</h5>
                                <input type="hidden" name="bonificacion_enganche" value="@{{nuve.bonificacionEngancheStr}}">
                            </div>
                        </div>
                        <div class="col-md-4 col-md-offset-8">
                            <div class="col-md-6">
                                <input class="form-control input-sm text-right" type="text" disabled value="Total:">
                            </div>
                            <div class="col-md-6">
                                <h5 class="text-right text-primary">@{{nuve.totalStr | currency : ""}}</h5>
                                <input type="hidden" name="total" value="@{{nuve.totalStr}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pull-right">
                <a href="#" class="btn btn-success" data-toggle="modal" data-target="#modalCancelar">Cancelar</a>
                <button type="submit" class="btn btn-success">Siguiente</button>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="modalCancelar" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Salir de Nueva Venta</h4>
            </div>
            <div class="modal-body">
                ¿Seguro desea salir de la venta?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">NO</button>
                <a href="{{url('/ventas')}}" class="btn btn-danger">SI</a>
            </div>
        </div>
    </div>
</div>
@stop @section('css')
<link rel="stylesheet" href="{{asset('librerias/select2/dist/css/select2.min.css')}}"> @stop @section('scripts')
<script src="{{asset('node_modules/bignumber.js/bignumber.min.js')}}"></script>
<script src="{{asset('librerias/select2/dist/js/select2.min.js')}}"></script>
<script>
$(function() {
    $('#cliente').select2();
    $('#articulo').select2();
});
</script>
@stop