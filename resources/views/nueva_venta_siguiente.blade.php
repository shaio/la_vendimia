@extends('index') @section('contenido')
<div class="row" ng-controller="nuevaVentaSiguienteController as nuve">
    {{-- Se almacena en angular la configuración actual del server --}}
    <div class="col-md-12" ng-init="nuve.setInfo({{$config}}, {{$folio}}, {{$cliente}}, {{$articulos}}, {{$enganche}}, {{$bonificacion_enganche}}, {{$total}})">
        <form action="{{url('/ventas/nueva/guardar')}}" method="POST">
            {{ csrf_field() }}
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Registro de Ventas</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <span class=" text-success pull-right">
                            <strong>Folio Venta: {{$folio}}</strong>
                            <input type="hidden" name="folio" value="{{$folio}}">
                        </span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="col-md-2">
                                <a href="#" class="btn btn-primary">Cliente</a>
                            </div>
                            <div class="col-md-5">
                                <input class="form-control" type="text" value="@{{nuve.cliente.clave}} - @{{nuve.cliente.nombre}} @{{nuve.cliente.apellido_paterno}} @{{nuve.cliente.apellido_materno}}" disabled>
                            </div>
                            <div class="col-md-5">
                                <strong>RFC: @{{nuve.cliente.rfc}}</strong>
                            </div>
                            <input type="hidden" name="cliente" value="@{{nuve.cliente}}">
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table">
                                <thead>
                                    <tr class="active">
                                        <th>Descripçión Artículo</th>
                                        <th>Modelo</th>
                                        <th>Cantidad</th>
                                        <th>Precio</th>
                                        <th>Importe</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="articulo in nuve.articulos">
                                        <td>@{{articulo.descripcion}}</td>
                                        <td>@{{articulo.modelo}}</td>
                                        <td>
                                            <input class="form-control" type="number" min="1" max="@{{articulo.existencia}}" value="@{{articulo.cantidad}}" disabled>
                                        </td>
                                        <td>@{{articulo.precioStr | currency : ""}}</td>
                                        <td>@{{articulo.importeStr | currency : ""}}</td>
                                        <input type="hidden" name="articulos[@{{$index}}]" value="@{{articulo}}" />
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-4 col-md-offset-8">
                            <div class="col-md-6">
                                <input class="form-control input-sm text-right" type="text" disabled value="Enganche:">
                            </div>
                            <div class="col-md-6">
                                <h5 class="text-right text-primary">@{{nuve.engancheStr | currency : ""}}</h5>
                                <input type="hidden" name="enganche" value="@{{nuve.engancheStr}}">
                            </div>
                        </div>
                        <div class="col-md-4 col-md-offset-8">
                            <div class="col-md-6">
                                <input class="form-control input-sm text-right" type="text" disabled value="Bonificación Enganche:">
                            </div>
                            <div class="col-md-6">
                                <h5 class="text-right text-primary">@{{nuve.bonificacionEngancheStr | currency : ""}}</h5>
                                <input type="hidden" name="bonificacion_enganche" value="@{{nuve.bonificacionEngancheStr}}">
                            </div>
                        </div>
                        <div class="col-md-4 col-md-offset-8">
                            <div class="col-md-6">
                                <input class="form-control input-sm text-right" type="text" disabled value="Total:">
                            </div>
                            <div class="col-md-6">
                                <h5 class="text-right text-primary">@{{nuve.totalStr | currency : ""}}</h5>
                                <input type="hidden" name="total" value="@{{nuve.totalStr}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped">
                                <thead>
                                    <tr class="info">
                                        <th colspan="5" class="text-center text-info">ABONOS MENSUALES</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="abono in nuve.abonos">
                                        <td>@{{abono.meses}} ABONOS DE</td>
                                        <td>@{{abono.importe_abono_str | currency}}</td>
                                        <td>TOTAL A PAGAR @{{abono.total_a_pagar_str | currency}}</td>
                                        <td>SE AHORRA @{{abono.importe_ahorra_str | currency}}</td>
                                        <td>
                                            <input type="radio" name="plazo" value="@{{abono}}" ng-model="nuve.plazo">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pull-right">
                <a href="#" class="btn btn-success" data-toggle="modal" data-target="#modalCancelar">Cancelar</a>
                <button type="submit" class="btn btn-success">Guardar</button>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="modalCancelar" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Salir de Nueva Venta</h4>
            </div>
            <div class="modal-body">
                ¿Seguro desea salir de la venta?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">NO</button>
                <a href="{{url('/ventas')}}" class="btn btn-danger">SI</a>
            </div>
        </div>
    </div>
</div>
@stop @section('scripts')
<script src="{{asset('node_modules/bignumber.js/bignumber.min.js')}}"></script>
@stop