@extends('index') @section('contenido')
<div class="row">
    <div class="col-md-12">
        <form id="formulario-nuevo-cliente" action="{{url('/clientes/nuevo/guardar')}}" method="POST">
            {{ csrf_field() }}
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Registro de Clientes</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <span class=" text-success pull-right">
                            <strong>Clave: {{$clave}}</strong>
                            <input type="hidden" name="clave" value="{{$clave}}">
                        </span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6 text-right">
                                        <label class="text-success" for="nombre">Nombre:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" name="nombre" ng-model="main.cliente_nombre">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6 text-right">
                                        <label class="text-success" for="apellido_paterno">Apellido Paterno:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" name="apellido_paterno" ng-model="main.cliente_apellido_paterno">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6 text-right">
                                        <label class="text-success" for="apellido_materno">Apellido Materno:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" name="apellido_materno" ng-model="main.cliente_apellido_materno">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6 text-right">
                                        <label class="text-success" for="rfc">RFC:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" name="rfc" ng-model="main.cliente_rfc">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pull-right ">
                <a href="#" class="btn btn-success" data-toggle="modal" data-target="#modalCancelar">Cancelar</a>
                <button type="submit " class="btn btn-success ">Guardar</button>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="modalCancelar" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Salir de Nuevo Cliente</h4>
            </div>
            <div class="modal-body">
                ¿Seguro desea salir de agregar un nuevo cliente?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">NO</button>
                <a href="{{url('/clientes')}}" class="btn btn-danger">SI</a>
            </div>
        </div>
    </div>
</div>
@stop