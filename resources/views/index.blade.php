<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>La Vendimia</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset('librerias/bootstrap/dist/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('librerias/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('librerias/toastr/toastr.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">
    @yield('css')
</head>

<body ng-app="mainApp" ng-controller="mainController as main">
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="navbar-header pull-right">
            <div class="navbar-header"><a class="navbar-brand" href="#">
                <span class="text-success">La Vendimia <i class="fa fa-ticket" aria-hidden="true"></i></span>
            </a></div>
        </div>
    </div>
    <div class="navbar navbar-inverse navbar-static-top" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Inicio <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/ventas')}}">Ventas</a></li>
                        <li class="divider"></li>
                        <li><a href="{{url('/clientes')}}">Clientes</a></li>
                        <li><a href="{{url('/articulos')}}">Artículos</a></li>
                        <li><a href="{{url('/configuracion')}}">Configuración</a></li>
                    </ul>
                </li>
            </ul>
            <p class="navbar-text pull-right">FECHA: {{$fecha}}</p>
        </div>
    </div>
    <div class="container">
        @yield('contenido')
    </div>
    <script src="{{asset('librerias/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('librerias/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('librerias/toastr/toastr.min.js')}}"></script>
    <script src="{{asset('librerias/angular/angular.min.js')}}"></script>
    {{-- JS de Angular --}}
    <script src="{{asset('js/angular/controllers/nuevaVentaCtrl.js')}}"></script>
    <script src="{{asset('js/angular/controllers/nuevaVentaSiguienteCtrl.js')}}"></script>
    <script src="{{asset('js/angular/controllers/mainCtrl.js')}}"></script>
    <script src="{{asset('js/angular/services/DataService.js')}}"></script>
    <script src="{{asset('js/angular/app.js')}}"></script>
    {{-- Evitar conflictos con JQuery --}}
    <script>
    $ = jQuery.noConflict();
    </script>
    @yield('scripts')
</body>

</html>