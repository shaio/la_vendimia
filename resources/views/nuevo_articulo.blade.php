@extends('index') @section('contenido')
<div class="row">
    <div class="col-md-12">
        <form id="formulario-nuevo-articulo" action="{{url('/articulos/nuevo/guardar')}}" method="POST">
            {{ csrf_field() }}
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Registro de Artículos</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <span class=" text-success pull-right">
                            <strong>Clave: {{$clave}}</strong>
                            <input type="hidden" name="clave" value="{{$clave}}">
                        </span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6 text-right">
                                        <label class="text-success" for="descripcion">Descripción:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" name="descripcion" ng-model="main.articulo_descripcion">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6 text-right">
                                        <label class="text-success" for="modelo">Modelo:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" name="modelo" ng-model="main.articulo_modelo">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6 text-right">
                                        <label class="text-success" for="precio">Precio:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control" min="0" step="0.01" type="number" name="precio" ng-model="main.articulo_precio">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6 text-right">
                                        <label class="text-success" for="existencia">Existencia:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control" min="0" step="1" type="number" name="existencia" ng-model="main.articulo_existencia">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pull-right ">
                <a href="#" class="btn btn-success" data-toggle="modal" data-target="#modalCancelar">Cancelar</a>
                <button type="submit " class="btn btn-success ">Guardar</button>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="modalCancelar" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Salir de Nuevo Artículo</h4>
            </div>
            <div class="modal-body">
                ¿Seguro desea salir de agregar un nuevo artículo?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">NO</button>
                <a href="{{url('/articulos')}}" class="btn btn-danger">SI</a>
            </div>
        </div>
    </div>
</div>
@stop