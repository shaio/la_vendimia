@extends('index') @section('contenido')
<div class="row">
    <div class="col-md-10">
        <h2 class="text-info">Clientes Registrados</h2>
    </div>
    <div class="col-md-2">
        <a href="{{url('/clientes/nuevo')}}" class="btn btn-info">
            <i class="fa fa-plus-circle fa-fw fa-lg text-success" aria-hidden="true"></i>Nuevo Cliente
        </a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
                <tr class="info">
                    <th>Clave Cliente</th>
                    <th colspan="2">Nombre</th>
                </tr>
            </thead>
            <tbody>
            	@foreach($clientes as $cliente)
                <tr>
					<td>{{$cliente->clave}}</td>  
					<td>{{$cliente->nombre}} {{$cliente->apellido_paterno}} {{$cliente->apellido_materno}}</td>
					<td>
						<a class="btn btn-sm btn-default" href="{{url('/clientes/consultar-editar')}}/{{$cliente->clave}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
					</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="text-center">
        {{$clientes->links()}}
    </div>
</div>
@stop