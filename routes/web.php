<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

Route::get('/ventas', 'VentasController@consultarVentasActivas');
Route::get('/ventas/nueva', 'VentasController@nuevaVenta');
Route::post('/ventas/nueva/siguiente', 'VentasController@nuevaVentaSiguiente');
Route::post('/ventas/nueva/guardar', 'VentasController@guardarNuevaVenta');
Route::get('/ventas/{folio_venta}', 'VentasController@consultarVenta');

Route::get('/clientes', 'ClientesController@consultarClientes');
Route::get('/clientes/nuevo', 'ClientesController@nuevoCliente');
Route::get('/clientes/consultar-editar/{clave}', 'ClientesController@consultarEditarCliente');
Route::post('/clientes/nuevo/guardar', 'ClientesController@guardarNuevoCliente');
Route::post('/clientes/actualizar', 'ClientesController@actualizarCliente');

Route::get('/articulos', 'ArticulosController@consultarArticulo');
Route::get('/articulos/nuevo', 'ArticulosController@nuevoArticulo');
Route::post('/articulos/nuevo/guardar', 'ArticulosController@guardarNuevoArticulo');
Route::get('/articulos/consultar-editar/{clave}', 'ArticulosController@consultarEditarArticulo');
Route::post('/articulos/actualizar', 'ArticulosController@actualizarArticulo');

Route::get('/configuracion', 'ConfigController@consultarEditarConfiguracion');
Route::post('/configuracion', 'ConfigController@guardarConfiguracion');