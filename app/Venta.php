<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
    protected $table = 'ventas';

	public function cliente() {
		return $this->hasOne('App\Cliente', 'id', 'id_cliente');
	}

	public function articulo() {
		return $this->belongsToMany('App\Articulo', 'ventas_articulos', 'id_venta', 'id_articulo')->withPivot('cantidad', 'precio', 'importe');
	}

}
