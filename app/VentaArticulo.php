<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentaArticulo extends Model
{
    protected $table = 'ventas_articulos';
}
