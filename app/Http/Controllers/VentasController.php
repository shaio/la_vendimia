<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Articulo;
use App\Cliente;
use App\Config;
use App\Venta;
use App\VentaArticulo;
use DB;

class VentasController extends Controller
{
    public function consultarVentasActivas () {
        $ventas = Venta::with('cliente')->orderBy('updated_at', 'desc')->paginate(10);
        
        $ventasArray = json_encode($ventas->items());

    	return view('ventas_activas', compact('ventas', 'ventasArray'));
    }

    public function nuevaVenta () {
    	$clientes = Cliente::all();
    	$articulos = Articulo::all();
    	$config = Config::find(1);

    	$folio = $this->obtenerFolio();

    	return view('nueva_venta', compact('folio', 'clientes', 'articulos', 'config'));
    }

    public function consultarVenta ($folio_venta) {
        $venta =  Venta::with('cliente', 'articulo')->where('folio', $folio_venta)->get()[0];

        return view('consultar_venta', compact('venta'));
    }

    public function nuevaVentaSiguiente (Request $data) {
        $config = Config::find(1);
        
        $folio = $data->folio;
        $cliente = $data->cliente;
        $articulos = json_encode($data->articulos);
        $enganche = $data->enganche;
        $bonificacion_enganche = $data->bonificacion_enganche;
        $total = $data->total;

        return view('nueva_venta_siguiente', compact('config', 'folio', 'cliente', 'articulos', 'enganche', 'bonificacion_enganche', 'total'));
    }

    public function guardarNuevaVenta (Request $data) {
        DB::beginTransaction();
        try {
            $cliente = (array) json_decode($data->cliente);
            $plazo = (array) json_decode($data->plazo);
            $articulos = $data->articulos;

            $venta = new Venta();

            $venta->folio = $data->folio;
            $venta->id_cliente = $cliente['id'];
            $venta->enganche = $data->enganche;
            $venta->bonificacion_enganche = $data->bonificacion_enganche;
            $venta->total = $data->total;
            $venta->abonos = $plazo['meses'];
            $venta->total_a_pagar = $plazo['total_a_pagar'];
            $venta->importe_abono = $plazo['importe_abono'];
            $venta->importe_ahorra = $plazo['importe_ahorra'];

            $venta->save();
            
            $ilen = count($articulos);
            for ($i = 0; $i < $ilen; $i++) {
                $articulo = (array) json_decode($articulos[$i]);

                $articuloBD = Articulo::find($articulo['id']);
                $articuloBD->existencia = $articuloBD->existencia - $articulo['cantidad'];
                $articuloBD->save();

                $venta_articulo = new VentaArticulo();
                $venta_articulo->id_venta = $venta->id;
                $venta_articulo->id_articulo = $articulo['id'];
                $venta_articulo->cantidad = $articulo['cantidad'];
                $venta_articulo->precio = $articulo['precio'];
                $venta_articulo->importe = $articulo['importe'];
                $venta_articulo->save();
            }

            DB::commit();
            $success = true;
        } catch (\Exception $e) {
            $success = false;
            DB::rollback();
        }

        if ($success) {
            return Redirect('/ventas');
        }
    }

    private function obtenerFolio () {
    	DB::beginTransaction();
    	try {
    		$config = Config::find(1);
    		$folio = $config->folio_ventas;
    		$config->folio_ventas = $folio + 1;
    		$config->save();
            
            DB::commit();
            $success = true;
        } catch (\Exception $e) {
            $success = false;
            DB::rollback();
        }

        if ($success) {
        	return $folio;
        }

    }
}
