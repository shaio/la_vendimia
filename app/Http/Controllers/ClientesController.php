<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;
use App\Config;
use DB;

class ClientesController extends Controller
{
    public function consultarClientes () {
    	$clientes = Cliente::paginate(10);

    	return view('clientes', compact('clientes'));
    }

    public function nuevoCliente () {
    	$clave = $this->obtenerClave();
    	
    	return view('nuevo_cliente', compact('clave'));
    }

    public function guardarNuevoCliente (Request $data) {
    	$cliente = new Cliente();

    	$cliente->clave = $data->clave;
    	$cliente->nombre = $data->nombre;
    	$cliente->apellido_paterno = $data->apellido_paterno;
    	$cliente->apellido_materno = $data->apellido_materno;
    	$cliente->rfc = $data->rfc;
    	$cliente->save();

        return Redirect('/clientes');
    }

    public function consultarEditarCliente($clave) {
        $cliente =  Cliente::where('clave', $clave)->get()[0];

        return view('consultar_editar_cliente', compact('cliente'));
    }

    public function actualizarCliente (Request $data) {
        $cliente = Cliente::where('clave', $data->clave)->get()[0];

        $cliente->nombre = $data->nombre;
    	$cliente->apellido_paterno = $data->apellido_paterno;
    	$cliente->apellido_materno = $data->apellido_materno;
    	$cliente->rfc = $data->rfc;

    	$cliente->save();

        return Redirect('/clientes');
    }

    private function obtenerClave () {
    	DB::beginTransaction();
    	try {
    		$config = Config::find(1);
    		$clave = $config->clave_clientes;
    		$config->clave_clientes = $clave + 1;
    		$config->save();
            
            DB::commit();
            $success = true;
        } catch (\Exception $e) {
            $success = false;
            DB::rollback();
        }

        if ($success) {
        	return $clave;
        }

    }
}
