<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Articulo;
use App\Config;
use DB;

class ArticulosController extends Controller
{
    public function consultarArticulo () {
    	$articulos = Articulo::paginate(10);

    	return view('articulos', compact('articulos'));
    }

    public function nuevoArticulo () {
    	$clave = $this->obtenerClave();
    	
    	return view('nuevo_articulo', compact('clave'));
    }

    public function guardarNuevoArticulo (Request $data) {
    	$articulo = new Articulo();

    	$articulo->clave = $data->clave;
    	$articulo->descripcion = $data->descripcion;
    	$articulo->modelo = $data->modelo;
    	$articulo->precio_articulo = $data->precio;
    	$articulo->existencia = $data->existencia;
    	$articulo->save();

        return Redirect('/articulos');
    }

    public function consultarEditarArticulo($clave) {
        $articulo =  Articulo::where('clave', $clave)->get()[0];

        return view('consultar_editar_articulo', compact('articulo'));
    }

    public function actualizarArticulo (Request $data) {
        $articulo = Articulo::where('clave', $data->clave)->get()[0];

        $articulo->descripcion = $data->descripcion;
    	$articulo->modelo = $data->modelo;
    	$articulo->precio_articulo = $data->precio;
    	$articulo->existencia = $data->existencia;

    	$articulo->save();

        return Redirect('/articulos');
    }

    private function obtenerClave () {
    	DB::beginTransaction();
    	try {
    		$config = Config::find(1);
    		$clave = $config->clave_articulos;
    		$config->clave_articulos = $clave + 1;
    		$config->save();
            
            DB::commit();
            $success = true;
        } catch (\Exception $e) {
            $success = false;
            DB::rollback();
        }

        if ($success) {
        	return $clave;
        }

    }
}
