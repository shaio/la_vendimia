<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Config;

class ConfigController extends Controller
{
    public function consultarEditarConfiguracion () {
    	$config = Config::find(1);

    	return view('config', compact('config'));
    }

    public function guardarConfiguracion (Request $data) {
    	$config = Config::find(1);

    	$config->tasa_financiamiento = $data->tasa_financiamiento;
    	$config->enganche = $data->enganche;
    	$config->plazo_maximo = $data->plazo_maximo;
    	$config->save();

        return Redirect('/configuracion');
    }
}
