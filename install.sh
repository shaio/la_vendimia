chmod -R 777 storage
composer install
cp .env.example .env
php artisan key:generate
bower install
npm install --prefix public/
echo "Editar el archivo .env y correr el comando:"
echo "php artisan migrate:refresh --seed"