var app = angular.module('nuevaVentaCtrl', []);

app.controller('nuevaVentaController', ['$scope', function($scope) {

    var vm = this;

    vm.articulos = [];
    vm.enganche = new BigNumber(0);
    vm.bonificacionEnganche = new BigNumber(0);
    vm.total = new BigNumber(0);
    vm.engancheStr = vm.enganche.toString();
    vm.bonificacionEngancheStr = vm.bonificacionEnganche.toString();
    vm.totalStr = vm.total.toString();

    vm.setConfig = function(config) {
        vm.config = config;
    };

    vm.agregarArticulo = function() {
        //Nuevo articulo se obtiene del select de la vista
        if (vm.nuevo_articulo != undefined) {
            var nuevoArticulo = JSON.parse(vm.nuevo_articulo);

            if (nuevoArticulo.existencia <= 0) {
                toastr.error("El artículo seleccionado no cuenta con existencia, favor de verificar", "Error");
            } else {
                for (var i = 0, ilen = vm.articulos.length; i < ilen; i++) {
                    if (vm.articulos[i].id == nuevoArticulo.id) {
                        toastr.warning("El artículo que intentas agregar ya está en el listado");
                        return;
                    }
                }

                nuevoArticulo.cantidad = 1;

                var precioArticulo = new BigNumber(nuevoArticulo.precio_articulo);
                var tasaFinanciamiento = new BigNumber(vm.config.tasa_financiamiento);
                var plazoMaximo = new BigNumber(vm.config.plazo_maximo);
                var tazaFinanciamientoPlusPlazoMaximo = tasaFinanciamiento.times(plazoMaximo);
                var divHundred = tazaFinanciamientoPlusPlazoMaximo.div(100);
                var onePlusDivHundred = new BigNumber(1).plus(divHundred);

                nuevoArticulo.precio = precioArticulo.times(onePlusDivHundred);
                nuevoArticulo.precioStr = nuevoArticulo.precio.toString();

                var indexArticulo = vm.articulos.push(nuevoArticulo) - 1;

                vm.calcularTotalesEImportes();
            }

            vm.nuevo_articulo = null;
        }
    };

    vm.eliminarArticulo = function(index) {
        vm.articulos.splice(index, 1);
        vm.calcularTotalesEImportes();
    };

    vm.calcularTotalesEImportes = function() {
        vm.enganche = new BigNumber(0);
        vm.bonificacionEnganche = new BigNumber(0);
        vm.total = new BigNumber(0);

        var porcentajeEnganche = new BigNumber(vm.config.enganche);
        var porcentajeEngancheDivHundred = porcentajeEnganche.div(100);

        var importes = new BigNumber(0);

        for (var i = 0, ilen = vm.articulos.length; i < ilen; i++) {
            var currentArticulo = vm.articulos[i];

            if (currentArticulo.cantidad == undefined)
                currentArticulo.cantidad = 0;

            var importe = currentArticulo.precio.times(currentArticulo.cantidad);
            currentArticulo.importe = importe;
            currentArticulo.importeStr = currentArticulo.importe.toString();

            importes = importes.plus(currentArticulo.importe);
        }

        var tasaFinanciamiento = new BigNumber(vm.config.tasa_financiamiento);
        var plazoMaximo = new BigNumber(vm.config.plazo_maximo);
        var tazaFinanciamientoPlusPlazoMaximo = tasaFinanciamiento.times(plazoMaximo);
        var tazaFinanciamientoPlusPlazoMaximoDivHundred = tazaFinanciamientoPlusPlazoMaximo.div(100);

        vm.enganche = importes.times(porcentajeEngancheDivHundred);
        vm.bonificacionEnganche = vm.enganche.times(tazaFinanciamientoPlusPlazoMaximoDivHundred);
        vm.total = importes.minus(vm.enganche).minus(vm.bonificacionEnganche);


        vm.engancheStr = vm.enganche.toString();
        vm.bonificacionEngancheStr = vm.bonificacionEnganche.toString();
        vm.totalStr = vm.total.toString();
    };

    jQuery(function() {
        jQuery('#cliente').on('change', function() {
            cliente_json = JSON.parse(this.value);
            vm.cliente_rfc = "RFC: " + cliente_json.rfc;
            $scope.$apply();
        });
        jQuery('.input-cantidad').on('input', function() {
            console.log('hola');
            vm.calcularTotalesEImportes();
            $scope.$apply();
        });

        jQuery("form").submit(function(event) {
            var cantidadCero = false;
            for (var i = 0, ilen = vm.articulos.length; i < ilen; i++) {
                if (vm.articulos[i].cantidad <= 0) {
                    cantidadCero = true;
                    break;
                }
            }

            if (vm.cliente == null ||
                vm.articulos.length <= 0 ||
                cantidadCero === true) {
                toastr.error("Los datos ingresados no son correctos, favor de verificar", "Error");
                event.preventDefault();
            }
        });
    });

}]);