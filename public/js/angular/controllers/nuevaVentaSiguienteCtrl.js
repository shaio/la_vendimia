var app = angular.module('nuevaVentaSiguienteCtrl', []);

app.controller('nuevaVentaSiguienteController', ['$timeout', function($timeout) {

    var vm = this;

    vm.articulos = [];
    vm.enganche = new BigNumber(0);
    vm.bonificacionEnganche = new BigNumber(0);
    vm.total = new BigNumber(0);
    vm.engancheStr = vm.enganche.toString();
    vm.bonificacionEngancheStr = vm.bonificacionEnganche.toString();
    vm.totalStr = vm.total.toString();
    vm.abonos = [];

    vm.setInfo = function(config, folio, cliente, articulos, enganche, bonificacionEnganche, total) {
        vm.config = config;
        vm.folio = folio;
        vm.cliente = cliente;
        vm.articulos = articulos;
        for (var i = 0, ilen = vm.articulos.length; i < ilen; i++) {
            vm.articulos[i] = JSON.parse(vm.articulos[i]);
            vm.articulos[i].precio = new BigNumber(vm.articulos[i].precio);
            vm.articulos[i].importe = new BigNumber(vm.articulos[i].importe);
            vm.articulos[i].precioStr = vm.articulos[i].precio.toString();
            vm.articulos[i].importeStr = vm.articulos[i].importe.toString();
        }
        vm.enganche = new BigNumber(enganche);
        vm.bonificacionEnganche = new BigNumber(bonificacionEnganche);
        vm.total = new BigNumber(total);

        vm.engancheStr = vm.enganche.toString();
        vm.bonificacionEngancheStr = vm.bonificacionEnganche.toString();
        vm.totalStr = vm.total.toString();

        /*console.log("config", config);
        console.log("folio", folio);
        console.log("cliente", cliente);
        console.log("articulos", vm.articulos);
        console.log("enganche", enganche);
        console.log("bonificacionEnganche", bonificacionEnganche);
        console.log("total", total);
        console.log("vm.engancheStr", vm.engancheStr);
        console.log("vm.bonificacionEngancheStr", vm.bonificacionEngancheStr);
        console.log("vm.totalStr", vm.totalStr);*/

        vm.calcularAbonos();
    };

    vm.calcularAbonos = function() {
        var tasaFinanciamiento = new BigNumber(vm.config.tasa_financiamiento);
        var plazoMaximo = new BigNumber(vm.config.plazo_maximo);
        var tasaFinanciamientoTimesPlazoMaximo = tasaFinanciamiento.times(plazoMaximo);
        var divHundred = tasaFinanciamientoTimesPlazoMaximo.div(100);
        var onePlusDivHundred = new BigNumber(1).plus(divHundred);

        var precioContado = vm.total.div(onePlusDivHundred);

        var meses = [3, 6, 9, 12];
        vm.abonos = [];

        for (var i = 0; i < 4; i++) {
            var tasaFinanciamientoTimesPlazo = tasaFinanciamiento.times(meses[i]);
            var tasaPlazoDivHundred = tasaFinanciamientoTimesPlazo.div(100);
            var onePlusTasaPlazoDivHundred = new BigNumber(1).plus(tasaPlazoDivHundred);

            var totalAPagar = precioContado.times(onePlusTasaPlazoDivHundred);
            var importeAbono = totalAPagar.div(meses[i]);
            var importeAhorra = vm.total.minus(totalAPagar);

            var abono = {
                'meses': meses[i],
                'total_a_pagar': totalAPagar,
                'total_a_pagar_str': totalAPagar.toString(),
                'importe_abono': importeAbono,
                'importe_abono_str': importeAbono.toString(),
                'importe_ahorra': importeAhorra,
                'importe_ahorra_str': importeAhorra.toString()
            };

            vm.abonos.push(abono);
        }
    };

    vm.notificacionFormularioCorrectoMostrada = false;
    jQuery(function() {
        jQuery("form").submit(function(event) {
            if (vm.plazo == undefined) {
                toastr.error("Debe seleccionar un plazo para realizar el pago de su compra", "Error");
                event.preventDefault();
            } else {
                if (vm.notificacionFormularioCorrectoMostrada === false) {
                    toastr.success("Tu venta ha sido registrada correctamente", "Bien Hecho!");
                    vm.notificacionFormularioCorrectoMostrada = true;
                    event.preventDefault();
                    $timeout(jQuery(this).submit(), 3000);
                }
            }
        });
    });

}]);