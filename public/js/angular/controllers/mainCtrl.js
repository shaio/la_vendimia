var app = angular.module('mainCtrl', ['DataService']);

app.controller('mainController', ['DataFactory', '$timeout', function(DataFactory, $timeout) {

    var vm = this;

    vm.hola = function(thin) {
        console.log("thin", thin);

    }

    vm.notificacionErrorCampo = function(campo) {
        toastr.error("No es posible continuar, debe ingresar " + campo + " es obligatorio", "Error");
    };

    vm.notificacionFormularioCorrectoMostrada = false;
    jQuery(function() {
        jQuery('#formulario-nuevo-cliente').submit(function(event) {
            if (!vm.cliente_nombre) {
                vm.notificacionErrorCampo('Nombre');
                event.preventDefault();
            } else if (!vm.cliente_apellido_paterno) {
                vm.notificacionErrorCampo('Apellido Paterno');
                event.preventDefault();
            } else if (!vm.cliente_apellido_materno) {
                vm.notificacionErrorCampo('Apellido Materno');
                event.preventDefault();
            } else if (!vm.cliente_rfc) {
                vm.notificacionErrorCampo('RFC');
                event.preventDefault();
            } else if (vm.notificacionFormularioCorrectoMostrada === false) {
                toastr.success("El cliente ha sido registrado correctamente", "Bien Hecho!");
                vm.notificacionFormularioCorrectoMostrada = true;
                event.preventDefault();
                $timeout(jQuery(this).submit(), 3000);
            }
        });
        jQuery('#formulario-nuevo-articulo').submit(function(event) {
            if (!vm.articulo_descripcion) {
                vm.notificacionErrorCampo('Descripción');
                event.preventDefault();
            } else if (!vm.articulo_precio) {
                vm.notificacionErrorCampo('Precio');
                event.preventDefault();
            } else if (!vm.articulo_existencia) {
                vm.notificacionErrorCampo('Existencia');
                event.preventDefault();
            } else if (vm.notificacionFormularioCorrectoMostrada === false) {
                toastr.success("El Artículo ha sido registrado correctamente", "Bien Hecho!");
                vm.notificacionFormularioCorrectoMostrada = true;
                event.preventDefault();
                $timeout(jQuery(this).submit(), 3000);
            }
        });
        jQuery('#formulario-configuracion').submit(function(event) {
            if (vm.notificacionFormularioCorrectoMostrada === false) {
                toastr.success("La configuración ha sido registrada", "Bien Hecho!");
                vm.notificacionFormularioCorrectoMostrada = true;
                event.preventDefault();
                $timeout(jQuery(this).submit(), 3000);
            }
        });
    });
}]);